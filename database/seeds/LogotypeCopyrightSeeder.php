<?php

use App\LogotypeCopyright;
use Illuminate\Database\Seeder;

class LogotypeCopyrightSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(LogotypeCopyright::class, 10)->create();
    }
}