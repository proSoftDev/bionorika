<?php

use App\DoctorVideoLesson;
use Illuminate\Database\Seeder;

class DoctorVideoLessonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(DoctorVideoLesson::class, 10)->create();
    }
}