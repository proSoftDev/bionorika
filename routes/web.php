<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('welcome');
});


Route::group(['prefix' => 'admin'], function () {
    Route::get('/ban', 'Admin\UserController@ban');
    Route::get('/ban-selected', 'Admin\UserController@banSelected');
    Route::get('/subscribe', 'Admin\UserController@subscribe');
    Route::get('/subscribe-selected', 'Admin\UserController@subscribeSelected');
    Route::get('/update-doctor-answers', 'Admin\DoctorScoreController@updateAnswers');
    Route::get('/update-pharmacist-answers', 'Admin\PharmacistScoreController@updateAnswers');
    Route::get('/export-user-information/{type}', 'Admin\UserController@exportInformation');
    Route::get('/export-user-statistics/{type}/{user_id}', 'Admin\UserController@exportStatistics');
    Voyager::routes();
});


//
//Route::get('/import_excel', 'ImportExcelController@index');
//Route::post('/import_excel/import', 'ImportExcelController@import');
//Route::post('/import_excel/delete', 'ImportExcelController@delete');




