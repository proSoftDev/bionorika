<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Pharmacovigilance extends Model
{
    use Translatable;
    protected $translatable = ['title', 'content', 'fax', 'phone', 'mail'];

    public static function getContent(){
        return self::first();
    }
}
