<?php

namespace App;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


class Subscription extends Model
{

    const isActive = 1;
    const isNotActive = 0;

    protected $fillable = ['user_id', 'year', 'month', 'status'];

    public function createNew($user_id){
        return self::create([
            'user_id' => $user_id,
            'year' => Carbon::now()->year,
            'month' => Carbon::now()->month,
            'status' => self::isActive,
        ]);
    }

    public static function getByUser($user_id, $months){
        return self::where(['user_id' => $user_id, 'year' => Carbon::now()->year])
            ->whereIn('month', $months)
            ->first();
    }

    public static function getAll($months){
        return self::where(['year' => Carbon::now()->year])
            ->whereIn('month', $months)
            ->get()->keyBy('user_id');
    }

}
