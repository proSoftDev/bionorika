<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 27.05.2020
 * Time: 12:45
 */

namespace App\Traits;


trait Testable
{

    public static function exists($id){
        return self::where('id', $id)->exists();
    }

    public static function getAttempt($id){
        return self::find($id)->attempts;
    }

}
