<?php

namespace App;

use App\Traits\Scorable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


class DoctorScore extends Model
{
    use Scorable;
    protected $fillable = [
        'test_id', 'user_id', 'score', 'passed_at'
    ];

    const isActive = 1;
    const isNotActive = 0;

    public static function checkAttempt($user_id, $test_id){

        $attempt =  self::where([['user_id', $user_id], ['test_id', $test_id]])->count();
        $test = DoctorTest::find($test_id);

        if($attempt >= $test->attempts){
            return self::isNotActive;
        }else{
            return self::isActive;
        }
    }


    public static function testAlreadyStarted($user_id, $test_id){

        $status = self::isNotActive;
        $test = DoctorTest::find($test_id);
        $model =  self::where([['user_id', $user_id], ['test_id', $test_id]])->get();
        foreach ($model as $v){
            if(DoctorScoreType::where('score_id', $v->id)->exists()) $status = self::isNotActive;
            elseif (Carbon::createFromFormat('Y-m-d H:i:s' ,$v->created_at)->timestamp +
                Carbon::createFromFormat('H:i:s' ,$test->test_time)->minute*60 > Carbon::now()->timestamp) {
                $status = self::isActive;
                break;
            }
        }

        return $status;
    }


    public static function checkStatus($score_id, $test){

        $status = self::isNotActive;
        if($model =  self::find($score_id)){
            if(DoctorScoreType::where('score_id', $score_id)->exists()) $status = self::isNotActive;
            elseif (Carbon::createFromFormat('Y-m-d H:i:s' ,$model->created_at)->timestamp +
                Carbon::createFromFormat('H:i:s' ,$test->test_time)->minute*60 > Carbon::now()->timestamp) {
                $status = self::isActive;
            }
        }

        return $status;
    }



    public function test(){
        return $this->hasOne('App\DoctorTest', 'id', 'test_id');
    }


    public function testName(){
        return $this->test ? $this->test->name : "";
    }


    public function attemptLeft($user_id){

        $test = DoctorTest::getAttempt($this->test_id);
        return $test - self::getTestAttempt($user_id, $this->test_id);
    }


}
