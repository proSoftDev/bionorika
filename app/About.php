<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class About extends Model
{
    use Translatable;
    protected $translatable = ['name', 'content'];


    public static function getAll(){
        return self::orderBy('sort', 'ASC')->select('id', 'name')->get();
    }

    public static function search($keyword){

        if(app()->getLocale() == 'ru') {
            $model = self::where('name', 'LIKE', "%{$keyword}%")
                ->orWhere('content', 'LIKE', "%{$keyword}%")
                ->select('id', 'name', 'content')
                ->get();
        }else {
            $model = self::whereTranslation('content', 'LIKE', "%{$keyword}%", [app()->getLocale()], false)
                ->select('id', 'name', 'content')
                ->get();
        }

        return $model;
    }

}
