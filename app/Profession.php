<?php

namespace App;

use App\Helpers\TranslatesCollection;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Profession extends Model
{

    use Translatable;
    protected $translatable = ['name'];

    public static function getAll(){
        return self::orderBy('sort', 'ASC')->select('id', 'name')->get();
    }

}
