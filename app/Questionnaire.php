<?php

namespace App;

use App\Traits\Cyclable;
use App\Traits\Scorable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Questionnaire extends Model
{
    use Translatable;
    use Cyclable;

    protected $translatable = ['question'];

    public static function getCurrentCycleData($cycle){
        return self::where(['year' => Carbon::now()->year, 'cycle' => $cycle])
            ->select('id', 'question')
            ->orderBy('sort', 'ASC')
            ->get();
    }

}
