<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class BannerFile extends Model
{

    public static function getAllByBanner($banner_id){
        return self::where('banner_id', $banner_id)->get();
    }
}
