<?php

namespace App\Http\Controllers;

use App\Mail\ExcelRequest;
use App\User;
use App\UserProfile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Hash;
use Rap2hpoutre\FastExcel\Facades\FastExcel;
use File;


class ImportExcelController extends Controller
{
    function index()
    {
        $data = User::get();
        return view('import_excel', compact('data'));
    }

    function import(Request $request)
    {
        $this->validate($request, array(
            'file'      => 'required'
        ));

        if($request->hasFile('file')){
            $extension = File::extension($request->file->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {

                $path = $request->file->getRealPath();
                $data = Excel::load($path)->noHeading()->get();
                $excel = [];

                if(!empty($data) && $data->count()) {
                    foreach ($data->toArray() as $key => $value) {

                        $name = $value[1];
                        $surname =  $value[2];
                        $profession =  $value[3];
                        $city =  $value[4];
                        $place_of_work = $value[5];
                        $email = $value[6];
                        $phone = $value[7];

                        $user = User::where(['email' => $email])->exists();
                        if($name != null && ($profession == 'Фармацевт' || $profession == 'Доктор') &&
                            $email != null && !$user && filter_var($email, FILTER_VALIDATE_EMAIL)){

                            $password = str_random(8);
                            if($profession == 'Фармацевт'){
                                $profession = 1;
                            }else{
                                $profession = 2;
                            }

                            $user = User::create([
                                'role_id' => 2,
                                'name' => $name.' '.$surname,
                                'email' => $email,
                                'password' => Hash::make($password),
                                'isActive' => 1,
                            ]);

                            UserProfile::create([
                                'profession_id' => $profession,
                                'city' => $city,
                                'phone' => $phone,
                                'place_of_work' => $place_of_work,
                                'first_password' => $password,
                                'user_id' => $user->id
                            ]);




                            $user->password = $password;
                            Mail::to($user->email)->send(new ExcelRequest($user));


                            if( count(Mail::failures()) > 0 ) {
                                $status = 0;
                            } else {
                                $status = 1;
                            }

                            $excel[] = ['Логин' => $email, 'Пароль' => $password, 'Статус сообщения' => $status];

                        }
                    }

                    $data = collect($excel);
                    $name = 'users.xlsx';
                    FastExcel::data($data)->export('excel/'.$name);
                    return redirect('excel/'.$name);

                }

            }
        }

        return back();

    }



    function delete(Request $request)
    {

        $this->validate($request, array(
            'file'      => 'required'
        ));

        if($request->hasFile('file')){
            $extension = File::extension($request->file->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {

                $path = $request->file->getRealPath();
                $data = Excel::load($path)->noHeading()->get();

                if(!empty($data) && $data->count()) {
                    foreach ($data->toArray() as $key => $value) {
                        $email = $value[6];
                        User::where('email', $email)->delete();
                    }

                }

            }
        }


        return back();
    }
}
