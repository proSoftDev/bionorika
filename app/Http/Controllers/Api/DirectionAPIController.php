<?php

namespace App\Http\Controllers\API;

use App\Direction;
use App\Helpers\TranslatesCollection;
use App\Http\Resources\DirectionCollection;
use App\Http\Resources\DirectionResource;
use App\Http\Controllers\Controller;

class DirectionAPIController extends Controller
{
    public function index()
    {
        $direction = Direction::getAll();
        TranslatesCollection::translate($direction, app()->getLocale());
        return new DirectionCollection($direction);
    }
 
    public function show(Direction $direction)
    {
        TranslatesCollection::translate($direction, app()->getLocale());
        return new DirectionResource($direction);
    }


}
