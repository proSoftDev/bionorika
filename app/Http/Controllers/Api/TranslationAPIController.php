<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 23.04.2020
 * Time: 19:24
 */

namespace App\Http\Controllers\API;


use App\Helpers\TranslatesCollection;
use App\Http\Controllers\Controller;
use App\TextTranslation;
use Illuminate\Http\Request;

class TranslationAPIController extends Controller
{

    public function translate(){

        $model = TextTranslation::get();
        TranslatesCollection::translate($model, app()->getLocale());
        $data = array();

        foreach ($model as $v){
            $data[$v->key] = $v->value;
        }

        return response()->json(['data' => $data], 200);
    }
}
