<?php

namespace App\Http\Controllers\API;

use App\DoctorVideoLesson;
use App\Helpers\TranslatesCollection;
use App\Http\Controllers\Controller;
use App\PharmacistVideoLesson;
use App\UserProfile;
use Illuminate\Support\Facades\Auth;

class VideoLessonAPIController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $profile = UserProfile::getProfileByID($user->id);

        if($profile->profession_id == UserProfile::isDoctor){
            $videoLesson = DoctorVideoLesson::getAllByDirection($profile->direction_id);
        }elseif($profile->profession_id == UserProfile::isPharmacist){
            $videoLesson = PharmacistVideoLesson::getAll();
        }else{
            return response(['Network does not exist'], 422);
        }

        TranslatesCollection::translate($videoLesson, app()->getLocale());
        return response()->json(['data' => $videoLesson], 200);
    }


}
