<?php

namespace App\Http\Controllers\API;

use App\Helpers\TranslatesCollection;
use App\Question;
use App\Http\Resources\QuestionCollection;
use App\Http\Resources\QuestionResource;
use App\Http\Controllers\Controller;
use App\UserProfile;
use Illuminate\Support\Facades\Auth;

class QuestionAPIController extends Controller
{
    public function index()
    {
        $profile = UserProfile::getProfileByID(Auth::id());
        if ($profile->profession_id == UserProfile::isDoctor) {
            $question = Question::getAll();
            TranslatesCollection::translate($question, app()->getLocale());
            return new QuestionCollection($question);
        }else{
            return response(['Network does not exist'], 422);
        }

    }
 
    public function show(Question $question)
    {
        $profile = UserProfile::getProfileByID(Auth::id());
        if ($profile->profession_id == UserProfile::isDoctor) {
            TranslatesCollection::translate($question, app()->getLocale());
            return new QuestionResource($question);
        }else{
            return response(['Network does not exist'], 422);
        }
    }


}
