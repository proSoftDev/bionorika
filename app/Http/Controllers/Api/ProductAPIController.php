<?php

namespace App\Http\Controllers\API;

use App\Helpers\TranslatesCollection;
use App\Product;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use TCG\Voyager\Facades\Voyager;

class ProductAPIController extends Controller
{
    public function index(Request $request)
    {
        $validateArray = [
            'catalog_id' => 'required|numeric',
        ];

        if($userValidate = $this->check($request, $validateArray)) {
            return $userValidate;
        }

        $product = Product::getAll($request->catalog_id);
        TranslatesCollection::translate($product, app()->getLocale());
        foreach ($product as $v) unset($v['description'], $v['indication_for_use'], $v['drug_properties'],
            $v['composition'], $v['dosage'], $v['faq'], $v['instruction_for_use']);

        return new ProductCollection($product);
    }
 
    public function show(Product $product)
    {
        $product->image = Voyager::image($product->image);
        $documents = $product->documents;
        TranslatesCollection::translate($documents, app()->getLocale());

        foreach ($documents as $document){
            unset($document['product_id'], $document['created_at'], $document['updated_at']);
            $document->file = Voyager::image(json_decode($document->file)[0]->download_link);}

        $product->documents = $documents;

        TranslatesCollection::translate($product, app()->getLocale());
        return new ProductResource($product);
    }

}
