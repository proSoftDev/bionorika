<?php

namespace App\Http\Controllers\API;

use App\FrequentlyAskedQuestion;
use App\Helpers\TranslatesCollection;
use App\Http\Resources\FrequentlyAskedQuestionCollection;
use App\Http\Controllers\Controller;
use App\Http\Resources\FrequentlyAskedQuestionResource;

class FrequentlyAskedQuestionAPIController extends Controller
{
    public function index()
    {
        $data = FrequentlyAskedQuestion::getAll();
        TranslatesCollection::translate($data, app()->getLocale());
        return new FrequentlyAskedQuestionCollection($data);
    }

    public function show(FrequentlyAskedQuestion $faq)
    {
        TranslatesCollection::translate($faq, app()->getLocale());
        return new FrequentlyAskedQuestionResource($faq);
    }

}
