<?php

namespace App\Http\Controllers\API;

use App\About;
use App\Helpers\TranslatesCollection;
use App\Http\Resources\AboutCollection;
use App\Http\Resources\AboutResource;
use App\Http\Controllers\Controller;

class AboutAPIController extends Controller
{
    public function index()
    {
        $about = About::getAll();
        TranslatesCollection::translate($about, app()->getLocale());
        foreach ($about as $v) unset($v['content']);

        return new AboutCollection($about);
    }
 
    public function show(About $about)
    {
        TranslatesCollection::translate($about, app()->getLocale());
        return new AboutResource($about);
    }


}
