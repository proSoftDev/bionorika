<?php

namespace App\Http\Controllers\API;

use App\Feedback;
use App\Mail\FeedbackRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class FeedbackAPIController extends Controller
{

    public function store(Request $request)
    {
        $validateArray = [
            'name'  => 'required|string',
            'phone' => 'required',
        ];

        if($errors = $this->check($request, $validateArray)) {
            return $errors;
        }

        $model = Feedback::create($request->all());
        Mail::to(setting('site.email'))->send(new FeedbackRequest($model));
        $data['text'] = trans('messages.Ваша заявка успешно отправлена');

        return response()->json(['data' => $data]);
    }

}
