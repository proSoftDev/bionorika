<?php

namespace App\Http\Controllers\API;

use App\Catalog;
use App\Helpers\TranslatesCollection;
use App\Http\Resources\CatalogCollection;
use App\Http\Resources\CatalogResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class CatalogAPIController extends Controller
{
    public function index()
    {
        $catalog = Catalog::getAll();
        TranslatesCollection::translate($catalog, app()->getLocale());
        return new CatalogCollection($catalog);
    }





}
