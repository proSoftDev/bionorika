<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 02.06.2020
 * Time: 10:39
 */

namespace App\Http\Controllers\API;


use App\Certificate;
use App\DoctorQuestionnaire;
use App\DoctorScore;
use App\DoctorTest;
use App\DoctorTestQuestion;
use App\Http\Controllers\Controller;
use App\PharmacistScore;
use App\PharmacistTest;
use App\PharmacistTestQuestion;
use App\Subscription;
use App\Traits\Cyclable;
use App\UserProfile;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ScaleAPIController extends Controller
{
    use Cyclable;

    const fromYear = 2020;
    const testMinPercent = 75;

    const firstCyclePercent = 35;
    const secondCyclePercent = 65;
    const thirdCyclePercent = 100;

    public function index(){

        $user_id = Auth::id();
        $profile = UserProfile::getProfileByID($user_id);
        $data = array();
        $scale = array();

        if($profile->profession_id == UserProfile::isDoctor){
            $maxBall = DoctorTest::maxBall;
            for($year = self::fromYear;$year <= Carbon::now()->year;$year++){
                $result = $this->checkDoctorCycleStatusAndGetData($user_id, $year);
                $scale[] = [
                    'year' => $year,
                    'percent' => $result['percent'],
                    'current_ball' => trans('messages.Общее количество фитобаллов на текущий момент', ['count' => $result['currentBall']]),
                    'max_ball' => trans('messages.Максимальное количество фитобаллов', ['count' => $maxBall]),
                    'scale' => $this->getContent(),
                ];

                if($year == Carbon::now()->year){
                    $status = $result['status'];
                }
            }
        }elseif($profile->profession_id == UserProfile::isPharmacist){
            $maxBall = PharmacistTest::maxBall;
            for($year = self::fromYear;$year <= Carbon::now()->year;$year++){
                $result = $this->checkPharmacistCycleStatusAndGetData($user_id, $year);
                $scale[] = [
                    'year' => $year,
                    'percent' => $result['percent'],
                    'current_ball' => trans('messages.Общее количество фитобаллов на текущий момент', ['count' => $result['currentBall']]),
                    'max_ball' => trans('messages.Максимальное количество фитобаллов', ['count' => $maxBall]),
                    'scale' => $this->getContent(),
                ];

                if($year == Carbon::now()->year){
                    $status = $result['status'];
                }
            }

        }else{
            return response(['Network does not exist'], 422);
        }

        $data['status'] = $status;
        $data['scale'] = $scale;

        return response()->json(['data' => $data], 200);
    }




    protected function getContent(){

        return [
            [
                'title' => trans('messages.Январь - Апрель'),
                'percent' => self::firstCyclePercent.'%',
                'cycle' => '1 '.trans('messages.цикл')
            ],
            [
                'title' => trans('messages.Май - Август'),
                'percent' => self::secondCyclePercent.'%',
                'cycle' => '2 '.trans('messages.цикл')
            ],
            [
                'title' => trans('messages.Сентябрь - Декабрь'),
                'percent' => self::thirdCyclePercent.'%',
                'cycle' => '3 '.trans('messages.цикл')
            ]
        ];
    }


    protected function checkDoctorCycleStatusAndGetData($user_id, $year){

        $months = $this->getMonths();
        $currentBall = 0;
        $percent = 0;
        $data['status'] = 1;

        foreach ($months as $k => $month){

            $testStatus = 0;

            if($year == self::fromYear && $k == 0){
                $testStatus = 1;
                $subscribeStatus = 1;
                $webinarStatus = 1;
            }else{

                // TEST
                foreach ($month as $v){

                    $score = DoctorScore::where('user_id', $user_id)
                        ->whereYear('created_at', '=', $year)
                        ->whereMonth('created_at', '=', $v)
                        ->first();

                    if($score){
                        $max_ball = DoctorScore::getMaxBall($user_id, $score->test_id);
                        $questionCount = DoctorTestQuestion::getCount($score->test_id);
                        $correctPercent = intval(($max_ball * 100) / $questionCount);

                        if($correctPercent >= self::testMinPercent && DoctorQuestionnaire::alreadyExist($score->test_id, $user_id)){
                            $testStatus = 1;
                        }
                    }
                }

                // SUBSCRIBE
                $subscribeStatus = $this->getSubscriptionStatus($user_id, $month);

                // WEBINAR
                $webinarStatus = $this->getWebinarStatus($user_id, $month);

            }

            if($testStatus == 1 && $subscribeStatus == 1 && $webinarStatus == 1){
                if($k == 0){
                    $percent = self::firstCyclePercent;
                    $currentBall += DoctorTest::firstCycleBall;
                }elseif($k == 1){
                    $percent = self::secondCyclePercent;
                    $currentBall += DoctorTest::secondCycleBall;
                }else{
                    $percent = self::thirdCyclePercent;
                    $currentBall += DoctorTest::thirdCycleBall;
                }

            }else{
                $data['status'] = 0;
                break;
            }
        }

        if($year == Carbon::now()->year){
            $data['status'] = 1;
        }

        $data['percent'] = $percent;
        $data['currentBall'] = $currentBall;

        return $data;
    }


    protected function checkPharmacistCycleStatusAndGetData($user_id, $year){

        $months = $this->getMonths();
        $currentBall = 0;
        $percent = 0;
        $data['status'] = 1;

        foreach ($months as $k => $month){

            $testStatus = 0;

            if($year == self::fromYear && $k == 0){
                $testStatus = 1;
                $subscribeStatus = 1;
            }else{

                // TEST
                foreach ($month as $v){

                    $score = PharmacistScore::where('user_id', $user_id)
                        ->whereYear('created_at', '=', $year)
                        ->whereMonth('created_at', '=', $v)
                        ->first();

                    if($score){
                        $max_ball = PharmacistScore::getMaxBall($user_id, $score->test_id);
                        $questionCount = PharmacistTestQuestion::getCount($score->test_id);
                        $correctPercent = intval(($max_ball * 100) / $questionCount);

                        if($correctPercent >= self::testMinPercent){
                            $testStatus = 1;
                        }
                    }
                }

                // SUBSCRIBE
                $subscribeStatus = $this->getSubscriptionStatus($user_id, $month);

            }

            if($testStatus == 1 && $subscribeStatus == 1){
                if($k == 0){
                    $percent = self::firstCyclePercent;
                    $currentBall += PharmacistTest::firstCycleBall;
                }elseif($k == 1){
                    $percent = self::secondCyclePercent;
                    $currentBall += PharmacistTest::secondCycleBall;
                }else{
                    $percent = self::thirdCyclePercent;
                    $currentBall += PharmacistTest::thirdCycleBall;
                }

            }else{
                $data['status'] = 0;
                break;
            }
        }

        if($year == Carbon::now()->year){
            $data['status'] = 1;
        }

        $data['percent'] = $percent;
        $data['currentBall'] = $currentBall;

        return $data;
    }


    protected function getSubscriptionStatus($user_id, $month){

        $subscribe = Subscription::getByUser($user_id, $month);
        if($subscribe && $subscribe->status == 1){
            return 1;
        }else{
            return 0;
        }
    }

    protected function getWebinarStatus($user_id, $month){

        $certificate = Certificate::getByUser($user_id, $month);
        if($certificate && $certificate->status == 1){
            return 1;
        }else{
            return 0;
        }
    }


}
