<?php

namespace App\Http\Controllers\API;

use App\Helpers\TranslatesCollection;
use App\Pharmacovigilance;
use App\Http\Resources\PharmacovigilanceResource;
use App\Http\Controllers\Controller;

class PharmacovigilanceAPIController extends Controller
{
    public function index()
    {
        $model = Pharmacovigilance::getContent();
        TranslatesCollection::translate($model, app()->getLocale());

        return new PharmacovigilanceResource($model);
    }

}
