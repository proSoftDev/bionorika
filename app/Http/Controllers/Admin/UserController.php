<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 31.03.2020
 * Time: 16:05
 */

namespace App\Http\Controllers\Admin;

use App\Certificate;
use App\DoctorScore;
use App\DoctorTest;
use App\Mail\UserAccessRequest;
use App\PharmacistScore;
use App\PharmacistTest;
use App\Subscription;
use App\Traits\Cyclable;
use App\User;
use App\UserProfile;
use App\UserQuestion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Facades\Voyager;
use Rap2hpoutre\FastExcel\Facades\FastExcel;
use TCG\Voyager\Http\Controllers\VoyagerUserController;


class UserController extends VoyagerUserController
{
    use Cyclable;

    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
         $slug= $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];

        $searchNames = [];
        if ($dataType->server_side) {
            $searchable = SchemaManager::describeTable(app($dataType->model_name)->getTable())->pluck('name')->toArray();
            $dataRow = Voyager::model('DataRow')->whereDataTypeId($dataType->id)->get();
            foreach ($searchable as $key => $value) {
                $displayName = $dataRow->where('field', $value)->first()->getTranslatedAttribute('display_name');
                $searchNames[$value] = $displayName ?: ucwords(str_replace('_', ' ', $value));
            }
        }

        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', $dataType->order_direction);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $query = $model->{$dataType->scope}();
            } else {
                $query = $model::select('*');
            }

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model)) && Auth::user()->can('delete', app($dataType->model_name))) {
                $usesSoftDeletes = true;

                if ($request->get('showSoftDeleted')) {
                    $showSoftDeleted = true;
                    $query = $query->withTrashed();
                }
            }

            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($search->value != '' && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($model);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'browse', $isModelTranslatable);

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->default_search_key ?? null;

        // Actions
        $actions = [];
        if (!empty($dataTypeContent->first())) {
            foreach (Voyager::actions() as $action) {
                $action = new $action($dataType, $dataTypeContent->first());

                if ($action->shouldActionDisplayOnDataType()) {
                    $actions[] = $action;
                }
            }
        }

        // Define showCheckboxColumn
        $showCheckboxColumn = false;
        if (Auth::user()->can('delete', app($dataType->model_name))) {
            $showCheckboxColumn = true;
        } else {
            foreach ($actions as $action) {
                if (method_exists($action, 'massAction')) {
                    $showCheckboxColumn = true;
                }
            }
        }

        // Define orderColumn
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + ($showCheckboxColumn ? 1 : 0);
            $orderColumn = [[$index, $sortOrder ?? 'desc']];
        }

        $view = 'voyager::bread.browse';

        if (view()->exists("voyager::$slug.browse")) {
            $view = "voyager::$slug.browse";
        }

        $subscription = Subscription::getAll($this->getMonthsAsArray());
        $certificate = Certificate::getAll($this->getMonthsAsArray());
        $profile = UserProfile::getAll();

        return Voyager::view($view, compact(
            'actions',
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchNames',
            'isServerSide',
            'defaultSearchKey',
            'usesSoftDeletes',
            'showSoftDeleted',
            'showCheckboxColumn',
            'subscription',
            'certificate',
            'profile'
        ));
    }

    public function show(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $isSoftDeleted = false;

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
            if ($dataTypeContent->deleted_at) {
                $isSoftDeleted = true;
            }
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');

        // Check permission
        $this->authorize('read', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'read', $isModelTranslatable);

        $view = 'voyager::bread.read';

        if (view()->exists("voyager::$slug.read")) {
            $view = "voyager::$slug.read";
        }

        $profile = UserProfile::getProfileByID($id);
        $questions = UserQuestion::getAnswersByID($id);

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'isSoftDeleted',
            'profile', 'questions'));
    }

    public function subscribe(Request $request)
    {
        if(Auth::user() != null && Auth::user()->role_id != 2 && Auth::user()->role_id != null){
            if ($request->user) {

                $user_id = $request->user;
                $subscribe = Subscription::getByUser($user_id, $this->getMonthsAsArray());

                if ($subscribe && $subscribe->status == 1) {
                    $subscribe->update(['status' => 0]);
                }else {
                    if($subscribe){
                        $subscribe->update(['status' => 1]);
                    }else{
                        $subscribe = new Subscription();
                        $subscribe->createNew($user_id);
                    }
                }
            }

            return back();
        }else{
            return abort(404);
        }

    }




    public function subscribeSelected(Request $request)
    {
        if(Auth::user() != null && Auth::user()->role_id != 2 && Auth::user()->role_id != null){
            if ($request->users) {
                $arr = explode(',', $request->users);
                foreach ($arr as $v) {

                    $user_id = $v;
                    $subscribe = Subscription::getByUser($user_id, $this->getMonthsAsArray());

                    if ($subscribe && $subscribe->status == 1) {
                        $subscribe->update(['status' => 0]);
                    } else {
                        if ($subscribe) {
                            $subscribe->update(['status' => 1]);
                        } else {
                            $subscribe = new Subscription();
                            $subscribe->createNew($user_id);
                        }
                    }
                }
            }

            return back();
        }else{
            return abort(404);
        }

    }



    public function ban(Request $request)
    {
        if(Auth::user() != null && Auth::user()->role_id != 2 && Auth::user()->role_id != null){
            if ($request->user) {
                $user = User::find($request->user);
                $profile = UserProfile::getProfileByID($request->user);
                if ($user->isActive) {
                    $user->update(['isActive' => 0]);
                } else {
                    if($profile->first_password == null){
                        $user->password = str_random(8);
                    }else{
                        $user->password = $profile->first_password;
                    }

                    Mail::to($user->email)->send(new UserAccessRequest($user));
                    $user->update(['isActive' => 1, 'password' => Hash::make($user->password)]);
                }
            }

            return back();
        }else{
            return abort(404);
        }

    }



    public function banSelected(Request $request)
    {
        if(Auth::user() != null && Auth::user()->role_id != 2 && Auth::user()->role_id != null){
            if ($request->users) {
                $arr = explode(',', $request->users);
                foreach ($arr as $v){
                    $user = User::find($v);
                    $profile = UserProfile::getProfileByID($v);
                    if ($user->isActive) {
                        $user->update(['isActive' => 0]);
                    } else {
                        if($profile->first_password == null){
                            $user->password = str_random(8);
                        }else{
                            $user->password = $profile->first_password;
                        }

                        Mail::to($user->email)->send(new UserAccessRequest($user));
                        $user->update(['isActive' => 1, 'password' => Hash::make($user->password)]);
                    }
                }
            }

            return back();
        }else{
            return abort(404);
        }

    }




    public function exportInformation($type){

        if(Auth::user() != null && Auth::user()->role_id != 2 && Auth::user()->role_id != null){

            $profiles = UserProfile::all();
            $certificate = Certificate::getAll($this->getMonthsAsArray());
            $list = [];

            if($type == 'all'){

                $name = 'Информация (Все) '.Carbon::now()->format('d-m-Y H m s').'.xlsx';

                foreach ($profiles as $v){
                    if($v->user != null && $v->user->isActive == 1){
                        $list[] = [
                            'Идентификатор' => $v->user->id,
                            'ФИО' => $v->user->name,
                            'Электронная почта' => $v->user->email,
                            'Профессия' => $v->professionName(),
                            'Направление' => $v->directionName(),
                            'Интерес' => $v->interestName(),
                            'Город' => $v->city,
                            'Номер телефона' => $v->phone,
                            'Место работы' => $v->place_of_work,
                            'Дата регистрация' => (string) $v->user->created_at
                        ];

                    }
                }



            }elseif($type == 'doctor'){

                $name = 'Информация (Доктор) '.Carbon::now()->format('d-m-Y H m s').'.xlsx';

                foreach ($profiles as $v){
                    if($v->user != null && $v->user->isActive == 1 && $v->profession_id == 2){
                        $list[] = [
                            'Идентификатор' => $v->user->id,
                            'ФИО' => $v->user->name,
                            'Электронная почта' => $v->user->email,
                            'Профессия' => $v->professionName(),
                            'Направление' => $v->directionName(),
                            'Интерес' => $v->interestName(),
                            'Город' => $v->city,
                            'Номер телефона' => $v->phone,
                            'Место работы' => $v->place_of_work,
                            'Сертификаты (в текущем цикле)' => isset($certificate[$v->user->id]) ? "Загружено" : "Не загружено",
                            'Дата регистрация' => (string) $v->user->created_at
                        ];
                    }
                }

            }elseif($type == 'pharmacist'){

                $name = 'Информация (Фармацевт) '.Carbon::now()->format('d-m-Y H m s').'.xlsx';

                foreach ($profiles as $v){
                    if($v->user != null && $v->user->isActive == 1 && $v->profession_id == 1){
                        $list[] = [
                            'Идентификатор' => $v->user->id,
                            'ФИО' => $v->user->name,
                            'Электронная почта' => $v->user->email,
                            'Профессия' => $v->professionName(),
                            'Интерес' => $v->interestName(),
                            'Город' => $v->city,
                            'Номер телефона' => $v->phone,
                            'Место работы' => $v->place_of_work,
                            'Дата регистрация' => (string) $v->user->created_at];
                    }
                }

            }else{
                abort(404);
            }


            $data = collect($list);
            FastExcel::data($data)->export('excel/'.$name);
            return redirect('excel/'.$name);

        }else{
            return abort(404);
        }

    }




    public function exportStatistics($type, $user_id){

        if(Auth::user() != null && Auth::user()->role_id != 2 && Auth::user()->role_id != null){

            $list = [];
            $certificate = Certificate::getAll($this->getMonthsAsArray());

            if($type == 'one'){

                $profile = UserProfile::where(['user_id' => $user_id])->first();
                $name = $profile->user->name.'_'.Carbon::now()->format('d-m-Y H m s').'.xlsx';


                if($profile->profession_id == UserProfile::isDoctor){
                    $tests = DoctorTest::getTestByDirection($profile->direction_id);
                }else{
                    $tests = PharmacistTest::get();
                }

                foreach ($tests as $v) {

                    if($profile->profession_id == UserProfile::isDoctor){
                        $max_ball_test = DoctorScore::getMaxBallTest($user_id, $v->id);
                        $used_attempt = DoctorScore::getTestAttempt($user_id, $v->id);
                    }else{
                        $max_ball_test = PharmacistScore::getMaxBallTest($user_id, $v->id);
                        $used_attempt = PharmacistScore::getTestAttempt($user_id, $v->id);
                    }

                    if($max_ball_test != null) {
                        $list[] = [
                            'Идентификатор' => $profile->user->id,
                            'ФИО' => $profile->user->name,
                            'Электронная почта' => $profile->user->email,
                            'Место работы' => $profile->place_of_work,
                            'Город' => $profile->city,
                            'Наименование теста' => $v->name,
                            'Количество попыток' => $used_attempt,
                            'Максимальный балл' => $max_ball_test->score,
                            'Дата прохождения теста' => (string)$max_ball_test->created_at,
                        ];
                    }
                }

            }elseif($type == 'doctor'){

                $name = 'Статистика (Доктор) '.Carbon::now()->format('d-m-Y H m s').'.xlsx';
                $profile = UserProfile::where(['profession_id' => UserProfile::isDoctor])->get();

                foreach ($profile as $val) {
                    $tests = DoctorTest::getTestByDirection($val->direction_id);
                    foreach ($tests as $v) {

                        $max_ball_test = DoctorScore::getMaxBallTest($val->user_id, $v->id);
                        $used_attempt = DoctorScore::getTestAttempt($val->user_id, $v->id);

                        if($max_ball_test != null) {
                            $list[] = [
                                'Идентификатор' => $val->user->id,
                                'ФИО' => $val->user->name,
                                'Электронная почта' => $val->user->email,
                                'Место работы' => $val->place_of_work,
                                'Город' => $val->city,
                                'Наименование теста' => $v->name,
                                'Количество попыток' => $used_attempt,
                                'Максимальный балл' => $max_ball_test->score,
                                'Дата прохождения теста' => (string)$max_ball_test->created_at,
                                'Сертификаты (в текущем цикле)' => isset($certificate[$val->user->id]) ? "Загружено" : "Не загружено",
                            ];
                        }
                    }
                }

            }elseif($type == 'pharmacist'){

                $name = 'Статистика (Фармацевт) '.Carbon::now()->format('d-m-Y H m s').'.xlsx';
                $profile = UserProfile::where(['profession_id' => UserProfile::isPharmacist])->get();

                foreach ($profile as $val) {

                    $tests = PharmacistTest::get();
                    foreach ($tests as $v) {

                        $max_ball_test = PharmacistScore::getMaxBallTest($val->user_id, $v->id);
                        $used_attempt = PharmacistScore::getTestAttempt($val->user_id, $v->id);

                        if($max_ball_test != null) {
                            $list[] = [
                                'Идентификатор' => $val->user->id,
                                'ФИО' => $val->user->name,
                                'Электронная почта' => $val->user->email,
                                'Место работы' => $val->place_of_work,
                                'Город' => $val->city,
                                'Наименование теста' => $v->name,
                                'Количество попыток' => $used_attempt,
                                'Максимальный балл' => $max_ball_test->score,
                                'Дата прохождения теста' => (string)$max_ball_test->created_at,
                            ];
                        }
                    }
                }

            }elseif($type == 'all'){

                $name = 'Статистика (Все) '.Carbon::now()->format('d-m-Y H m s').'.xlsx';
                $profile = UserProfile::where(['profession_id' => UserProfile::isPharmacist])->get();

                foreach ($profile as $val) {

                    $tests = PharmacistTest::get();
                    foreach ($tests as $v) {

                        $max_ball_test = PharmacistScore::getMaxBallTest($val->user_id, $v->id);
                        $used_attempt = PharmacistScore::getTestAttempt($val->user_id, $v->id);

                        if($max_ball_test != null) {
                            $list[] = [
                                'Идентификатор' => $val->user->id,
                                'ФИО' => $val->user->name,
                                'Электронная почта' => $val->user->email,
                                'Профессия' => $val->professionName(),
                                'Место работы' => $val->place_of_work,
                                'Город' => $val->city,
                                'Наименование теста' => $v->name,
                                'Количество попыток' => $used_attempt,
                                'Максимальный балл' => $max_ball_test->score,
                                'Дата прохождения теста' => (string)$max_ball_test->created_at,
                                'Сертификаты (в текущем цикле)' => "",
                            ];
                        }
                    }
                }


                $profile = UserProfile::where(['profession_id' => UserProfile::isDoctor])->get();
                foreach ($profile as $val) {

                    $tests = DoctorTest::getTestByDirection($val->direction_id);
                    foreach ($tests as $v) {

                        $max_ball_test = DoctorScore::getMaxBallTest($val->user_id, $v->id);
                        $used_attempt = DoctorScore::getTestAttempt($val->user_id, $v->id);

                        if($max_ball_test != null) {
                            $list[] = [
                                'Идентификатор' => $val->user->id,
                                'ФИО' => $val->user->name,
                                'Электронная почта' => $val->user->email,
                                'Профессия' => $val->professionName(),
                                'Место работы' => $val->place_of_work,
                                'Город' => $val->city,
                                'Наименование теста' => $v->name,
                                'Количество попыток' => $used_attempt,
                                'Максимальный балл' => $max_ball_test->score,
                                'Дата прохождения теста' => (string)$max_ball_test->created_at,
                                'Сертификаты (в текущем цикле)' => isset($certificate[$val->user->id]) ? "Загружено" : "Не загружено",
                            ];
                        }
                    }
                }

            }else{
                abort(404);
            }


            $data = collect($list);
            FastExcel::data($data)->export('excel/'.$name);
            return redirect('excel/'.$name);

        }else{
            return abort(404);
        }

    }


}
