<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use App\Jobs\SendCode;
use App\Mail\NewPasswordRequest;
use App\User;
use App\UserCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class ForgotPasswordController extends Controller
{


    public function sendCode(Request $request)
    {
        $validateArray = ['email' => ['required', 'string', 'email', 'exists:users'],];

        if($userValidate = $this->check($request, $validateArray)) {
            return $userValidate;
        }

        $user = User::getUserByEmail($request->email);
        SendCode::dispatch($user);

        return response(['user_id' => $user->id ,'message' => trans('messages.An email has been sent to you!')], 200);
    }



    public function checkCode(Request $request)
    {

        $validateArray = [
            'code' => ['required', 'integer'],
            'user_id' => ['required', 'integer'],
        ];

        if($userValidate = $this->check($request, $validateArray)) {
            return $userValidate;
        }

        $user = $this->findUserByCode($request->user_id, $request->code);
        if ($user) {

            UserCode::where('user_id', $user->id)->delete();

            $user->password = str_random(8);
            Mail::to($user->email)->send(new NewPasswordRequest($user->password));
            $user->update(['password' => Hash::make($user->password)]);

            $token = $user->createToken('Laravel Password Grant Client')->accessToken;
            return response(['token' => $token]);

        } else {
            return response(['error' => trans('messages.Invalid code')], 401);
        }
    }




    private function findUserByCode($user_id, $code)
    {
        $check = UserCode::where('user_id', $user_id)->whereRaw('DATEDIFF(created_at, now()) < 3')
            ->orderBy('id', 'desc')
            ->first();

        if ($check != null && $check->code == $code) {
            return User::find($check->user_id);
        } else
            return false;
    }
}
