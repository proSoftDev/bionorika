<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use TCG\Voyager\Facades\Voyager;


class NewsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name'              => $this->name,
            'image'             => Voyager::image($this->image),
            'full_description'  => $this->full_description,
            'meta_title'        => $this->meta_title,
            'meta_description'  => $this->meta_description,
            'meta_keyword'      => $this->meta_keyword,
        ];
    }
}
