<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Page extends Model
{
    use Translatable;
    protected $translatable = ['meta_title', 'meta_description', 'meta_keyword'];

    public static function findByUrl($url){
        return self::where('url', $url)->select('meta_title', 'meta_description', 'meta_keyword')->first();
    }
}
