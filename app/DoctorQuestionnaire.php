<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class DoctorQuestionnaire extends Model
{
    protected $fillable = [
        'test_id', 'user_id', 'questionnaire_id', 'answer'
    ];

    public function createNew($test_id, $user_id, $questionnaire_id, $answer){
        return self::create([
            'test_id' => $test_id,
            'user_id' => $user_id,
            'questionnaire_id' => $questionnaire_id,
            'answer' => $answer
        ]);
    }

    public static function alreadyExist($test_id, $user_id){
        return self::where(['test_id' => $test_id, 'user_id' => $user_id])->exists();
    }
}
