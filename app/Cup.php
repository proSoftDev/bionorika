<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\Translatable;


class Cup extends Model
{
    use Translatable;
    protected $translatable = ['name', 'description'];

    public static function getMinYearOfCompletion(){
        return self::min('year_of_completion');
    }

    public static function getMaxYearOfCompletion(){
        return self::max('year_of_completion');
    }

    public static function getByCompletion($completion){
        $model = self::where('year_of_completion', $completion)
            ->select('name', 'description', 'image')
            ->first();

        $model->image = Voyager::image($model->image);
        return $model;
    }
}
