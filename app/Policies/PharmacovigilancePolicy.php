<?php

namespace App\Policies;

use App\User;
use App\Pharmacovigilance;
use Illuminate\Auth\Access\HandlesAuthorization;

class PharmacovigilancePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any pharmacovigilance.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the pharmacovigilance.
     *
     * @param  App\User  $user
     * @param  App\Pharmacovigilance  $pharmacovigilance
     * @return bool
     */
    public function view(User $user, Pharmacovigilance $pharmacovigilance)
    {
        return false;
    }

    /**
     * Determine whether the user can create a pharmacovigilance.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the pharmacovigilance.
     *
     * @param  App\User  $user
     * @param  App\Pharmacovigilance  $pharmacovigilance
     * @return bool
     */
    public function update(User $user, Pharmacovigilance $pharmacovigilance)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the pharmacovigilance.
     *
     * @param  App\User  $user
     * @param  App\Pharmacovigilance  $pharmacovigilance
     * @return bool
     */
    public function delete(User $user, Pharmacovigilance $pharmacovigilance)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the pharmacovigilance.
     *
     * @param  App\User  $user
     * @param  App\Pharmacovigilance  $pharmacovigilance
     * @return bool
     */
    public function restore(User $user, Pharmacovigilance $pharmacovigilance)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the pharmacovigilance.
     *
     * @param  App\User  $user
     * @param  App\Pharmacovigilance  $pharmacovigilance
     * @return bool
     */
    public function forceDelete(User $user, Pharmacovigilance $pharmacovigilance)
    {
        return false;
    }
}
