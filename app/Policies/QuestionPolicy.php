<?php

namespace App\Policies;

use App\User;
use App\Question;
use Illuminate\Auth\Access\HandlesAuthorization;

class QuestionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any question.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the question.
     *
     * @param  App\User  $user
     * @param  App\Question  $question
     * @return bool
     */
    public function view(User $user, Question $question)
    {
        return false;
    }

    /**
     * Determine whether the user can create a question.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the question.
     *
     * @param  App\User  $user
     * @param  App\Question  $question
     * @return bool
     */
    public function update(User $user, Question $question)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the question.
     *
     * @param  App\User  $user
     * @param  App\Question  $question
     * @return bool
     */
    public function delete(User $user, Question $question)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the question.
     *
     * @param  App\User  $user
     * @param  App\Question  $question
     * @return bool
     */
    public function restore(User $user, Question $question)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the question.
     *
     * @param  App\User  $user
     * @param  App\Question  $question
     * @return bool
     */
    public function forceDelete(User $user, Question $question)
    {
        return false;
    }
}
