<?php

namespace App\Policies;

use App\User;
use App\Certificate;
use Illuminate\Auth\Access\HandlesAuthorization;

class CertificatePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any certificate.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the certificate.
     *
     * @param  App\User  $user
     * @param  App\Certificate  $certificate
     * @return bool
     */
    public function view(User $user, Certificate $certificate)
    {
        return false;
    }

    /**
     * Determine whether the user can create a certificate.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the certificate.
     *
     * @param  App\User  $user
     * @param  App\Certificate  $certificate
     * @return bool
     */
    public function update(User $user, Certificate $certificate)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the certificate.
     *
     * @param  App\User  $user
     * @param  App\Certificate  $certificate
     * @return bool
     */
    public function delete(User $user, Certificate $certificate)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the certificate.
     *
     * @param  App\User  $user
     * @param  App\Certificate  $certificate
     * @return bool
     */
    public function restore(User $user, Certificate $certificate)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the certificate.
     *
     * @param  App\User  $user
     * @param  App\Certificate  $certificate
     * @return bool
     */
    public function forceDelete(User $user, Certificate $certificate)
    {
        return false;
    }
}
