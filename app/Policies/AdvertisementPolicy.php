<?php

namespace App\Policies;

use App\User;
use App\Advertisement;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdvertisementPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any advertisement.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the advertisement.
     *
     * @param  App\User  $user
     * @param  App\Advertisement  $advertisement
     * @return bool
     */
    public function view(User $user, Advertisement $advertisement)
    {
        return false;
    }

    /**
     * Determine whether the user can create a advertisement.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the advertisement.
     *
     * @param  App\User  $user
     * @param  App\Advertisement  $advertisement
     * @return bool
     */
    public function update(User $user, Advertisement $advertisement)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the advertisement.
     *
     * @param  App\User  $user
     * @param  App\Advertisement  $advertisement
     * @return bool
     */
    public function delete(User $user, Advertisement $advertisement)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the advertisement.
     *
     * @param  App\User  $user
     * @param  App\Advertisement  $advertisement
     * @return bool
     */
    public function restore(User $user, Advertisement $advertisement)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the advertisement.
     *
     * @param  App\User  $user
     * @param  App\Advertisement  $advertisement
     * @return bool
     */
    public function forceDelete(User $user, Advertisement $advertisement)
    {
        return false;
    }
}
