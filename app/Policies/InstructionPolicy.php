<?php

namespace App\Policies;

use App\User;
use App\Instruction;
use Illuminate\Auth\Access\HandlesAuthorization;

class InstructionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any instruction.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the instruction.
     *
     * @param  App\User  $user
     * @param  App\Instruction  $instruction
     * @return bool
     */
    public function view(User $user, Instruction $instruction)
    {
        return false;
    }

    /**
     * Determine whether the user can create a instruction.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the instruction.
     *
     * @param  App\User  $user
     * @param  App\Instruction  $instruction
     * @return bool
     */
    public function update(User $user, Instruction $instruction)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the instruction.
     *
     * @param  App\User  $user
     * @param  App\Instruction  $instruction
     * @return bool
     */
    public function delete(User $user, Instruction $instruction)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the instruction.
     *
     * @param  App\User  $user
     * @param  App\Instruction  $instruction
     * @return bool
     */
    public function restore(User $user, Instruction $instruction)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the instruction.
     *
     * @param  App\User  $user
     * @param  App\Instruction  $instruction
     * @return bool
     */
    public function forceDelete(User $user, Instruction $instruction)
    {
        return false;
    }
}
