<?php

namespace App\Policies;

use App\User;
use App\Banner;
use Illuminate\Auth\Access\HandlesAuthorization;

class BannerPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any banner.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the banner.
     *
     * @param  App\User  $user
     * @param  App\Banner  $banner
     * @return bool
     */
    public function view(User $user, Banner $banner)
    {
        return false;
    }

    /**
     * Determine whether the user can create a banner.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the banner.
     *
     * @param  App\User  $user
     * @param  App\Banner  $banner
     * @return bool
     */
    public function update(User $user, Banner $banner)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the banner.
     *
     * @param  App\User  $user
     * @param  App\Banner  $banner
     * @return bool
     */
    public function delete(User $user, Banner $banner)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the banner.
     *
     * @param  App\User  $user
     * @param  App\Banner  $banner
     * @return bool
     */
    public function restore(User $user, Banner $banner)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the banner.
     *
     * @param  App\User  $user
     * @param  App\Banner  $banner
     * @return bool
     */
    public function forceDelete(User $user, Banner $banner)
    {
        return false;
    }
}
