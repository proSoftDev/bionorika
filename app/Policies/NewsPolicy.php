<?php

namespace App\Policies;

use App\User;
use App\News;
use Illuminate\Auth\Access\HandlesAuthorization;

class NewsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any news.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the news.
     *
     * @param  App\User  $user
     * @param  App\News  $news
     * @return bool
     */
    public function view(User $user, News $news)
    {
        return false;
    }

    /**
     * Determine whether the user can create a news.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the news.
     *
     * @param  App\User  $user
     * @param  App\News  $news
     * @return bool
     */
    public function update(User $user, News $news)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the news.
     *
     * @param  App\User  $user
     * @param  App\News  $news
     * @return bool
     */
    public function delete(User $user, News $news)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the news.
     *
     * @param  App\User  $user
     * @param  App\News  $news
     * @return bool
     */
    public function restore(User $user, News $news)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the news.
     *
     * @param  App\User  $user
     * @param  App\News  $news
     * @return bool
     */
    public function forceDelete(User $user, News $news)
    {
        return false;
    }
}
