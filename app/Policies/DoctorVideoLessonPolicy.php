<?php

namespace App\Policies;

use App\User;
use App\DoctorVideoLesson;
use Illuminate\Auth\Access\HandlesAuthorization;

class DoctorVideoLessonPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any doctorVideoLesson.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the doctorVideoLesson.
     *
     * @param  App\User  $user
     * @param  App\DoctorVideoLesson  $doctorVideoLesson
     * @return bool
     */
    public function view(User $user, DoctorVideoLesson $doctorVideoLesson)
    {
        return false;
    }

    /**
     * Determine whether the user can create a doctorVideoLesson.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the doctorVideoLesson.
     *
     * @param  App\User  $user
     * @param  App\DoctorVideoLesson  $doctorVideoLesson
     * @return bool
     */
    public function update(User $user, DoctorVideoLesson $doctorVideoLesson)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the doctorVideoLesson.
     *
     * @param  App\User  $user
     * @param  App\DoctorVideoLesson  $doctorVideoLesson
     * @return bool
     */
    public function delete(User $user, DoctorVideoLesson $doctorVideoLesson)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the doctorVideoLesson.
     *
     * @param  App\User  $user
     * @param  App\DoctorVideoLesson  $doctorVideoLesson
     * @return bool
     */
    public function restore(User $user, DoctorVideoLesson $doctorVideoLesson)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the doctorVideoLesson.
     *
     * @param  App\User  $user
     * @param  App\DoctorVideoLesson  $doctorVideoLesson
     * @return bool
     */
    public function forceDelete(User $user, DoctorVideoLesson $doctorVideoLesson)
    {
        return false;
    }
}
