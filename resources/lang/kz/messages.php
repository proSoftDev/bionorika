<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'question is required'                          => ':number сұрақ толтырылуы міндетті',
    'answer is required'                            => ':number сұраққа жауап қажет',
    'soon the results will be come out'             => 'Жауаптарыңыз модераторларға жіберілді.',
    'ready'                                         => 'Рахмет, ',
    'your score'                                    => 'Сіз :quantity-дан :correct ұпай жинадыңыз',
    'not available'                                 => 'Қол жетімді емес',
    'attempt'                                       => 'Талпыныс №',
    'not used'                                      => 'ҚолданылмаҒан',
    'An email has been sent to you!'                => 'Сізге электронды пошта жіберілді!',
    'Invalid code'                                  => 'Жарамсыз код!',
    'Search result found'                           => ':keyword бойынша іздеу нәтижелері',
    'Search result not found'                       => ':keyword бойынша ештеңе табылмады',
    'video order'                                   => '№:num бейне сабақ',
    'Неправильный логин или пароль'                 => 'Логин немесе құпия сөз қате.',
    'Ваша заявка успешно отправлена'                => 'Сіздің өтінішіңіз сәтті жіберілді!',
    'Январь - Апрель'                               => 'Қаңтар - сәуір',
    'Май - Август'                                  => 'Мамыр - тамыз',
    'Сентябрь - Декабрь'                            => 'Қыркүйек - желтоқсан',
    'цикл'                                          => 'цикл',
    'Общее количество фитобаллов на текущий момент' => 'Қазіргі уақытта фитобаллдардың жалпы саны :count',
    'Максимальное количество фитобаллов'            => 'Фитоболдардың максималды саны :count'
];
