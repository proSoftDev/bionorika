<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'question is required'                          => 'Question :number is required',
    'answer is required'                            => 'Answer for the :number question is required',
    'soon the results will be come out'             => 'Your answers have been sent to moderators.',
    'ready'                                         => 'Thank you, ',
    'your score'                                    => 'You scored :correct points out of :quantity',
    'not available'                                 => 'Not available',
    'attempt'                                       => 'Attempt #',
    'not used'                                      => 'Not used',
    'An email has been sent to you!'                => 'An email has been sent to you!',
    'Invalid code'                                  => 'Invalid code!',
    'Search result found'                           => 'Search results for request :keyword',
    'Search result not found'                       => 'For query: keyword nothing found',
    'video order'                                   => 'Video lecture #:num',
    'Неправильный логин или пароль'                 => 'Incorrect login or password.',
    'Ваша заявка успешно отправлена'                => 'Your application has been sent successfully!',
    'Январь - Апрель'                               => 'January - April',
    'Май - Август'                                  => 'May - August',
    'Сентябрь - Декабрь'                            => 'September - December',
    'цикл'                                          => 'cycle',
    'Общее количество фитобаллов на текущий момент' => 'Total number of phytoballs at the moment :count',
    'Максимальное количество фитобаллов'            => 'Maximum number of phytoballs :count',

];
