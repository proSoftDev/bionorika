

$('[data-get-items-field=doctor_score_belongsto_user_relationship]').prop('disabled', true);
$('[data-get-items-field=doctor_score_belongsto_doctor_test_relationship]').prop('disabled', true);


$('[data-get-items-field=pharmacist_score_belongsto_user_relationship]').prop('disabled', true);
$('[data-get-items-field=pharmacist_score_belongsto_pharmacist_test_relationship]').prop('disabled', true);

if($('[name=profession_id]').val() == 1){
    $('[name=direction_id]').closest('div').hide();
}

$('body').on('change', '[name=profession_id]', function () {
    if($('[name=profession_id]').val() == 1){
        $('[name=direction_id]').closest('div').hide();
    }else{
        $('[name=direction_id]').closest('div').show();
    }
});



// Banner
myCheckBox = $('[name=type]');
video = $('[name=video]');
image = $('[name=image]');

if(myCheckBox.is(':checked')){
    image.prop('disabled', true);
}else{
    video.prop('disabled', true);
}

myCheckBox.on("change", function (e) {
    e.preventDefault();
    if(myCheckBox.is(':checked')){
        video.prop('disabled', false);
        image.prop('disabled', true);
    }else{
        video.prop('disabled', true);
        image.prop('disabled', false);
    }
});
// end banner


$('[name=row_id]').click(function () {


    var checkBoxList = $('[name=row_id]');
    var checkBoxSelectedItems = new Array();
    var ary="";

    for (var i = 0; i < checkBoxList.length; i++) {
        if (checkBoxList[i].checked) {
            checkBoxSelectedItems.push(checkBoxList[i].value);
            ary +=    checkBoxList[i].value+","
        }
    }

    ary = ary.slice(0,-1);
    document.getElementById('ban-selected').href= '/admin/ban-selected?users='+ary;
    document.getElementById('subscribe-selected').href= '/admin/subscribe-selected?users='+ary;

    if($(this).attr("checked") == 'checked') {


        $(this).attr("checked", false);
        // not checked


    } else {

        $(this).attr("checked", true);
        // checked
    }


});
